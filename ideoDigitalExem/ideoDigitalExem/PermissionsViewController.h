//
//  PermissionsViewController.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PermissionsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *requestAccessButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;

@end
