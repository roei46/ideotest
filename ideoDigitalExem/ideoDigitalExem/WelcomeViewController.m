//
//  WelcomeViewController.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "WelcomeViewController.h"
#import "ViewControllerSwitch.h"
#import "LocationHelper.h"
#import "AppUtils.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)start:(id)sender {
        [AppUtils saveDefaultValue:@"YES" forKey:DEFAULTS_KEY_HAS_VISITED_APP];
        
        //skip the permissions screen if location permission has been granted.
        NSString *nextVcName = [[LocationHelper sharedInstance] hasPermission] ? @"HomeViewController" : @"PermissionsViewController";
        UIViewController *nextVc = [self.storyboard instantiateViewControllerWithIdentifier:nextVcName];
        
        [ViewControllerSwitch loadController:nextVc withOptions:UIViewAnimationOptionTransitionCrossDissolve];
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
