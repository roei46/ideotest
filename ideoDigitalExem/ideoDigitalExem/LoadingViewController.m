//
//  ViewController.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "LoadingViewController.h"
#import "ViewControllerSwitch.h"
#import "LocationHelper.h"
#import "AppUtils.h"
#import <AFNetworking/AFNetworking.h>

@interface LoadingViewController ()

@end

@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self enterApplication];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)enterApplication {
    NSString *nextVcName;
    if ([AppUtils hasVisitedApp]) {
        nextVcName = [[LocationHelper sharedInstance] hasPermission] ? @"HomeViewController" : @"PermissionsViewController";
    } else {
        nextVcName = @"WelcomeViewController";
    }
    
    UIViewController *nextVc = [self.storyboard instantiateViewControllerWithIdentifier:nextVcName];
    
    [ViewControllerSwitch loadController:nextVc withOptions:UIViewAnimationOptionTransitionCrossDissolve];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
