//
//  OpenWeatherMapAPI.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 09/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "OpenWeatherMapAPI.h"
#import <AFNetworking/AFNetworking.h>
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AlertHelper.h"

@implementation OpenWeatherMapAPI


+(void)requestFirstCityData:(void (^)(NSDictionary * _Nonnull))success failure:(void (^)(NSError * _Nullable))failure {
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@",OPENWEATHERMAP_API_KEY,WEATHER_FIRST_CITY,WEATHER_TEMPERATURE_UNIT,WEATHERMAP_API_KEY];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
}

+(void)requestSecontCityData:(void (^)(NSDictionary * _Nonnull))success failure:(void (^)(NSError * _Nullable))failure {
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@",OPENWEATHERMAP_API_KEY,WEATHER_SECOND_CITY,WEATHER_TEMPERATURE_UNIT,WEATHERMAP_API_KEY];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
    
}

+(void)requestReverseGeocodeForLocation:(CLLocation *)location success:(void (^)(NSDictionary * _Nonnull))success failure:(void (^)(NSError * _Nullable))failure {
    NSString *latLng = [NSString stringWithFormat:@"lat=%f&lon=%f", location.coordinate.latitude, location.coordinate.longitude];
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@",OPENWEATHERMAP_API_KEY,latLng,WEATHER_TEMPERATURE_UNIT,WEATHERMAP_API_KEY];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
}


+(void)downloadImageWithUrl:(NSString * _Nonnull)urlPath
                    success:(nullable void (^)(UIImage * _Nonnull))success
                    failure:(nullable void (^)(NSError * _Nullable))failure{
    
    NSURL *url = [[NSURL alloc]initWithString:urlPath];

    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager loadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        //
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        success(image);
    }];
}

+(void)requestThirdCityData:(void (^)(NSDictionary * _Nonnull))success failure:(void (^)(NSError * _Nullable))failure {
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@",OPENWEATHERMAP_API_KEY,WEATHER_THIRD_CITY,WEATHER_TEMPERATURE_UNIT,WEATHERMAP_API_KEY];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
    
}

@end
