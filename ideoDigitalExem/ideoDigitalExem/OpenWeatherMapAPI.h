//
//  OpenWeatherMapAPI.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 09/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface OpenWeatherMapAPI : NSObject

+(void)downloadImageWithUrl:(NSString * _Nonnull)urlPath
                    success:(nullable void (^)(UIImage * _Nonnull result))success
                    failure:(nullable void (^)(NSError * _Nullable e))failure;

+(void)requestReverseGeocodeForLocation:(CLLocation * _Nonnull)location
                                success:(nullable void(^)(NSDictionary * _Nonnull result))success
                                failure:(nullable void(^)(NSError * _Nullable e))failure;

+(void)requestFirstCityData:(nullable void(^)(NSDictionary * _Nonnull result))success
                             failure:(nullable void(^)(NSError * _Nullable e))failure;


+(void)requestSecontCityData:(nullable void(^)(NSDictionary * _Nonnull result))success
                  failure:(nullable void(^)(NSError * _Nullable e))failure;


+(void)requestThirdCityData:(nullable void(^)(NSDictionary * _Nonnull result))success
                  failure:(nullable void(^)(NSError * _Nullable e))failure;
@end
