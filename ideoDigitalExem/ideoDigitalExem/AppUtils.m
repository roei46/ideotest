//
//  AppUtils.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 08/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "AppUtils.h"
#import <UIKit/UIKit.h>
#import "LocationHelper.h"
#import "NSDictionary+Ideo.h"

@implementation AppUtils



static AppUtils *sharedUtils = nil;
static dispatch_once_t onceToken;
static NSMutableDictionary *plistDictionary;
static NSDictionary *defaultLocalConfig;
static BOOL isSetup = NO;

+(AppUtils *) sharedUtils
{
    dispatch_once(&onceToken, ^{
        sharedUtils = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedUtils setup];
        isSetup = YES;
    }
    return sharedUtils;
}

-(void)setup {
    [self initLocalConfig];
}

- (void) initLocalConfig {
    // Load configuration from settings.plist if exists
    NSString *pathToPlist = [[NSBundle mainBundle] pathForResource:@"PVPSettings" ofType:@"plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathToPlist]) {
        NSLog(@"Local configuration file settings.plist found in the main app bundle.");
        plistDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pathToPlist];
    } else {
        plistDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    }
    
    defaultLocalConfig = @{CONFIG_KEY_MONITOR_REACHABILITY: @(YES),
                           DEFAULTS_KEY_HAS_VISITED_APP: @([[AppUtils getDefaultValueFofKey:DEFAULTS_KEY_HAS_VISITED_APP] boolValue])
                           };
    
}


+ (void)fillModalVcForCurrentLocation:(ModalViewController *)modalVc{
    
    
    NSArray *description = [[LocationHelper sharedInstance].current safeValueForKeyPath:@"weather"];
    NSString *des = [[description firstObject]valueForKey:@"description"];
    NSString *temp = [[[LocationHelper sharedInstance].current safeValueForKeyPath:@"main.temp"]stringValue];
    NSString *celsius = [NSString stringWithFormat:@"%@ \u00B0C", temp];
    
    modalVc.nameString = [LocationHelper sharedInstance].current[@"name"];
    modalVc.descriptionString = des;
    modalVc.temperatureString = celsius;
    modalVc.prssureString = [[[LocationHelper sharedInstance].current safeValueForKeyPath:@"main.pressure"]stringValue];
    modalVc.humidityString = [[[LocationHelper sharedInstance].current safeValueForKeyPath:@"main.humidity"]stringValue];
    modalVc.windString = [[[LocationHelper sharedInstance].current safeValueForKeyPath:@"wind.speed"]stringValue];
}

+ (void)fillModalVcForFirstLocation:(ModalViewController *)modalVc{
    
    
    NSArray *description = [[LocationHelper sharedInstance].defualtFirstLocation safeValueForKeyPath:@"weather"];
    NSString *des = [[description firstObject]valueForKey:@"description"];
    NSString *temp = [[[LocationHelper sharedInstance].defualtFirstLocation safeValueForKeyPath:@"main.temp"]stringValue];
    NSString *celsius = [NSString stringWithFormat:@"%@ \u00B0C", temp];
    
    modalVc.nameString = [LocationHelper sharedInstance].defualtFirstLocation[@"name"];
    modalVc.descriptionString = des;
    modalVc.temperatureString = celsius;
    modalVc.prssureString = [[[LocationHelper sharedInstance].defualtFirstLocation safeValueForKeyPath:@"main.pressure"]stringValue];
    modalVc.humidityString = [[[LocationHelper sharedInstance].defualtFirstLocation safeValueForKeyPath:@"main.humidity"]stringValue];
    modalVc.windString = [[[LocationHelper sharedInstance].defualtFirstLocation safeValueForKeyPath:@"wind.speed"]stringValue];
}

+ (void)fillModalVcForSecondLocation:(ModalViewController *)modalVc{
    
    
    NSArray *description = [[LocationHelper sharedInstance].defualtSecondLocation safeValueForKeyPath:@"weather"];
    NSString *des = [[description firstObject]valueForKey:@"description"];
    NSString *temp = [[[LocationHelper sharedInstance].defualtSecondLocation safeValueForKeyPath:@"main.temp"]stringValue];
    NSString *celsius = [NSString stringWithFormat:@"%@ \u00B0C", temp];
    
    modalVc.nameString = [LocationHelper sharedInstance].defualtSecondLocation[@"name"];
    modalVc.descriptionString = des;
    modalVc.temperatureString = celsius;
    modalVc.prssureString = [[[LocationHelper sharedInstance].defualtSecondLocation safeValueForKeyPath:@"main.pressure"]stringValue];
    modalVc.humidityString = [[[LocationHelper sharedInstance].defualtSecondLocation safeValueForKeyPath:@"main.humidity"]stringValue];
    modalVc.windString = [[[LocationHelper sharedInstance].defualtSecondLocation safeValueForKeyPath:@"wind.speed"]stringValue];
}

+ (void)fillModalVcForThirdLocation:(ModalViewController *)modalVc{
    
    
    NSArray *description = [[LocationHelper sharedInstance].defualtThirdLocation safeValueForKeyPath:@"weather"];
    NSString *des = [[description firstObject]valueForKey:@"description"];
    NSString *temp = [[[LocationHelper sharedInstance].defualtThirdLocation safeValueForKeyPath:@"main.temp"]stringValue];
    NSString *celsius = [NSString stringWithFormat:@"%@ \u00B0C", temp];
    
    modalVc.nameString = [LocationHelper sharedInstance].defualtThirdLocation[@"name"];
    modalVc.descriptionString = des;
    modalVc.temperatureString = celsius;
    modalVc.prssureString = [[[LocationHelper sharedInstance].defualtThirdLocation safeValueForKeyPath:@"main.pressure"]stringValue];
    modalVc.humidityString = [[[LocationHelper sharedInstance].defualtThirdLocation safeValueForKeyPath:@"main.humidity"]stringValue];
    modalVc.windString = [[[LocationHelper sharedInstance].defualtThirdLocation safeValueForKeyPath:@"wind.speed"]stringValue];
}

+ (NSString *)getLocalConfiguration:(NSString *)name {
    if (!plistDictionary) {
        [[self sharedUtils] initLocalConfig];
    }
    return plistDictionary[name] ? plistDictionary[name] : defaultLocalConfig[name];
}

+ (void)setLocalConfiguration:(NSString *)configuration forKey:(NSString *)key {
    if (!plistDictionary) {
        [[self sharedUtils] initLocalConfig];
    }
    plistDictionary[key] = configuration;
}

+(void) saveDefaultValue:(NSString *)value forKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

+(NSString *) getDefaultValueFofKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:key];
}

+(void) performSelector:(SEL)selector on:(id)delegate {
    if ([delegate respondsToSelector:selector]) {
        IMP imp = [delegate methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(delegate, selector);
    } else {
        NSLog(@"Method: %@ missing for delegate: %@", NSStringFromSelector(selector), NSStringFromClass([delegate class]));
    }
}

+(void) performSelector:(SEL)selector on:(id)delegate withObject:(id)object {
    if ([delegate respondsToSelector:selector]) {
        IMP imp = [delegate methodForSelector:selector];
        void (*func)(id, SEL, id) = (void *)imp;
        func(delegate, selector, object);
    } else {
        NSLog(@"Method: %@ missing for delegate: %@", NSStringFromSelector(selector), NSStringFromClass([delegate class]));
    }
}

+ (BOOL)hasVisitedApp {
    return [[self getLocalConfiguration:DEFAULTS_KEY_HAS_VISITED_APP] boolValue];
}

@end
