//
//  LocationHelper.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "LocationHelper.h"
#import "AlertHelper.h"
#import "OpenWeatherMapAPI.h"
#import "NSDictionary+Ideo.h"

@interface LocationHelper () <CLLocationManagerDelegate>

@property(nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, assign) BOOL isFirstTime;


@end

@implementation LocationHelper


static LocationHelper *sharedInstance = nil;
static dispatch_once_t onceToken;
static BOOL isSetup = NO;

+(LocationHelper *) sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void)setup {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
}

#pragma mark permission handling

/*
 * Checks if the location permission has been properly configured in the Info.plist and authorized.
 */
-(BOOL)hasPermission {
    if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
        return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways;
    } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
        return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse;
    } else {
        // no permission message in the info.plist. throw an exception?
        NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
        return NO;
    }
}

/*
 * Checks if the location permission was denied, or turned off manually in the settings app.
 */
-(BOOL)permissionDenied {
    return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted
    || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied;
}

/*
 * Display a dialog to redirect the user to the settings app in case the location permission is denied or blocked manually.
 */
-(void)handlePermissionDenied {
    [AlertHelper showAlertWithTitle:TEXT_PERMISSION_RESTRICTED_TITLE
                         andMessage:TEXT_PERMISSION_RESTRICTED_MESSAGE
              withCancelButtonTitle:TEXT_MAYBE_LATER
               withOtherButtonTitle:TEXT_OPEN_SETTINGS
                        withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
                            if (confirmed) {
                                //                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                UIApplication *application = [UIApplication sharedApplication];
                                [application openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
                                
                                
                            }
                        }];
}

-(void)requestPermission {
    if ([self permissionDenied]) {
        [self handlePermissionDenied];
    } else if (![self hasPermission] && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
        // in case we switch between permission types
        if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
            [self.locationManager requestAlwaysAuthorization];
        } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            [self.locationManager  requestWhenInUseAuthorization];
        } else {
            // no permission message in the info.plist. throw an exception?
        }
    }
}

-(void)start {
    [self requestPermission];
    [self.locationManager startUpdatingLocation];
}

-(void)stop {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark CLLocationManagerDelegate methods
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_PERMISSION_STATE_CHANGE object:nil userInfo:@{}];
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self start];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {

    if (locations.count > 0) {
        _currentLocation = locations[0];
        NSLog(@"Got location: %f, %f", _currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude);
        [self stop];
        [NSTimer scheduledTimerWithTimeInterval:20.0
                                         target:self
                                       selector:@selector(getLocationData)
                                       userInfo:nil
                                        repeats:YES];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]) {
            [self getLocationData];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"HasLaunchedOnce"];
        }

    }
}


-(void)getLocationData{
    
    [OpenWeatherMapAPI requestReverseGeocodeForLocation:_currentLocation success:^(NSDictionary * _Nonnull result) {
        
        _current = result;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_CHANGED object:nil userInfo:@{}];
        
        [self start];
    } failure:^(NSError * _Nullable e) {
                [AlertHelper showAlertWithTitle:@"Error" andMessage:e.localizedDescription];
    }];
}


@end
