//
//  ModalViewController.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *temp;
@property (weak, nonatomic) IBOutlet UILabel *des;
@property (weak, nonatomic) IBOutlet UILabel *humidity;
@property (weak, nonatomic) IBOutlet UILabel *prssure;
@property (weak, nonatomic) IBOutlet UILabel *windSpeed;
@property (weak, nonatomic) NSString *nameString;
@property (weak, nonatomic) NSString *temperatureString;
@property (weak, nonatomic) NSString *descriptionString;
@property (weak, nonatomic) NSString *humidityString;
@property (weak, nonatomic) NSString *prssureString;
@property (weak, nonatomic) NSString *windString;




@end
