//
//  FirstCityViewController.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "FirstCityViewController.h"
#import "LocationHelper.h"
#import "OpenWeatherMapAPI.h"
#import "Constants.h"
#import "NSDictionary+Ideo.h"
#import "ModalViewController.h"
#import "AppUtils.h"
#import "AlertHelper.h"
#import "ViewCity.h"

@interface FirstCityViewController ()<loadModalDelegate>

@end

@implementation FirstCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationNameHasChanged:) name:NOTIFICATION_FIRST_CITY object:nil];
    ViewCity *customNib = [[ViewCity alloc] initWithFrame:[UIScreen mainScreen].bounds];
    customNib.delegate = self;
    [self.view addSubview:customNib];
    
    [OpenWeatherMapAPI requestFirstCityData:^(NSDictionary * _Nonnull result) {
        [LocationHelper sharedInstance].defualtFirstLocation = result;
        //        [self fillLabels];
        //        [self fillImage];
        [customNib fillLabelsWithData:result];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_FIRST_CITY object:nil userInfo:@{}];
        
    } failure:^(NSError * _Nullable e) {
        [AlertHelper showAlertWithTitle:@"Error" andMessage:e.localizedDescription];
    }];
}

-(void)loadNewScreen{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    ModalViewController *modalViewController = (ModalViewController*)[mainStoryboard
                                                                      instantiateViewControllerWithIdentifier: @"ModalViewController"];
    [AppUtils fillModalVcForFirstLocation:modalViewController];
    
    [self presentViewController:modalViewController animated:YES completion:nil];
    
}


- (void)locationNameHasChanged:(NSNotification *)notification {
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
