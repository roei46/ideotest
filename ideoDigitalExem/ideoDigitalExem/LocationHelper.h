//
//  LocationHelper.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface LocationHelper : NSObject

@property (strong, nonatomic, readonly) CLLocation *currentLocation;

@property (strong, nonatomic) NSDictionary *current;
@property (strong, nonatomic) NSDictionary *defualtFirstLocation;
@property (strong, nonatomic) NSDictionary *defualtSecondLocation;
@property (strong, nonatomic) NSDictionary *defualtThirdLocation;



@property (strong, nonatomic, readonly) NSArray <NSDictionary *> *places;
@property (strong, nonatomic) NSString *placesType;

+(LocationHelper *) sharedInstance;
-(BOOL)hasPermission;
-(void)requestPermission;
-(void)start;
-(void)stop;
@end
