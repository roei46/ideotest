//
//  ViewCity.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 15/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "ViewCity.h"
#import "LocationHelper.h"
#import "NSDictionary+Ideo.h"
#import "OpenWeatherMapAPI.h"
#import "ModalViewController.h"
#import "AppUtils.h"

@implementation ViewCity



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self customInit];
        
        return self;
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self customInit];
        
    }
    
    return self;
}

- (void)customInit
{
    // load the nib
    
    [[NSBundle mainBundle] loadNibNamed:@"ViewCity" owner:self options:nil];
    
    // add the super view from the nib
    
    [self addSubview: _contentView];
    self.contentView.frame = self.bounds;
}

- (void)fillLabelsWithData:(NSDictionary *)data{
    NSArray *description = [data safeValueForKeyPath:@"weather"];
    NSString *des = [[description firstObject]valueForKey:@"description"];
    
    NSString *temp = [[data safeValueForKeyPath:@"main.temp"]stringValue];
    NSString *celsius = [NSString stringWithFormat:@"%@ \u00B0C", temp];
    _locationNameLabel.text = [data safeValueForKeyPath:@"name"];
    _temp.text =  celsius;
    _des.text = des;
    
    
    NSArray *images = [[LocationHelper sharedInstance].defualtFirstLocation safeValueForKeyPath:@"weather"];
    NSString *icon = [[images firstObject]valueForKey:@"icon"];
    
    NSString *url = [NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",icon];
    [self fillImageWithUrl:url];
    
}

-(void)fillImageWithUrl:(NSString *)url{
    
    [OpenWeatherMapAPI downloadImageWithUrl:url success:^(UIImage * _Nonnull result) {
        [_img setImage:result];
    } failure:^(NSError * _Nullable e) {
    }];
    
}

- (IBAction)moreInfo:(id)sender {
    
    [self.delegate loadNewScreen];
    
}


@end
