//
//  ModalViewController.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController ()

@end

@implementation ModalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self fillLabelsWithParam];
}

-(void)fillLabelsWithParam{
    
    _locationNameLabel.text = _nameString;
    _temp.text = [@"Temperature is : " stringByAppendingString:_temperatureString];
    _humidity.text = [[@"Humidity is : " stringByAppendingString:_humidityString ]stringByAppendingString:@" %"];
    _windSpeed.text = [[@"Wind speed is : " stringByAppendingString:_windString]stringByAppendingString:@" hPa"];
    _prssure.text = [[@" Pressure is : " stringByAppendingString:_prssureString]stringByAppendingString:@" meter/sec"];
    _des.text = _descriptionString;
    
}
- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
