//
//  AppUtils.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "ModalViewController.h"


@interface AppUtils : NSObject

+(AppUtils *) sharedUtils;

+ (void)fillModalVcForCurrentLocation:(ModalViewController *)modalVc;

+ (void)fillModalVcForFirstLocation:(ModalViewController *)modalVc;

+ (void)fillModalVcForSecondLocation:(ModalViewController *)modalVc;

+ (void)fillModalVcForThirdLocation:(ModalViewController *)modalVc;

+(NSString *) getLocalConfiguration:(NSString *)name;

+(void) setLocalConfiguration:(NSString *)configuration forKey:(NSString *)key;

+(void) saveDefaultValue:(NSString *)value forKey:(NSString *)key;

+(NSString *) getDefaultValueFofKey:(NSString *)key;

+(void) performSelector:(SEL)selector on:(id)delegate;

+(void) performSelector:(SEL)selector on:(id)delegate withObject:(id)object;

+(BOOL)hasVisitedApp;

@end
