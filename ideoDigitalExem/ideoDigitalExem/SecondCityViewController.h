//
//  SecondCityViewController.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 12/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondCityViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *temp;
@property (weak, nonatomic) IBOutlet UILabel *des;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@end
