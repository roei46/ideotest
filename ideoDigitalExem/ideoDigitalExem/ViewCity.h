//
//  ViewCity.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 15/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol loadModalDelegate <NSObject>

-(void)loadNewScreen;

@end

@interface ViewCity : UIView
@property (weak, nonatomic) IBOutlet UILabel *locationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *temp;
@property (weak, nonatomic) IBOutlet UILabel *des;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIView *contentView;


- (void)fillLabelsWithData:(NSDictionary *)data;


@property (nonatomic, weak) id<loadModalDelegate> delegate;


@end
