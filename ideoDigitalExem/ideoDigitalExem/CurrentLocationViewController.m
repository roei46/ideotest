//
//  CurrentLocationViewController.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "CurrentLocationViewController.h"
#import "LocationHelper.h"
#import "OpenWeatherMapAPI.h"
#import "Constants.h"
#import "NSDictionary+Ideo.h"
#import "ModalViewController.h"
#import "AppUtils.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CurrentLocationViewController ()
@property (strong, nonatomic) NSDictionary *current;

@end

@implementation CurrentLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationNameHasChanged:) name:NOTIFICATION_LOCATION_CHANGED object:nil];
}

- (IBAction)moreInfo:(id)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    ModalViewController *modalViewController = (ModalViewController*)[mainStoryboard
                                                                       instantiateViewControllerWithIdentifier: @"ModalViewController"];
    [AppUtils fillModalVcForCurrentLocation:modalViewController];
    
    [self presentViewController:modalViewController animated:YES completion:nil];
}
- (void)locationNameHasChanged:(NSNotification *)notification {
    
    [self fillLabels];
    [self fillImage];

}

-(void)fillImage{
    NSArray *images = [[LocationHelper sharedInstance].current safeValueForKeyPath:@"weather"];
    NSString *icon = [[images firstObject]valueForKey:@"icon"];
    
    NSString *url = [NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",icon];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator startAnimating];
    [indicator setCenter:self.view.center ];
    [self.view addSubview:indicator];
    [OpenWeatherMapAPI downloadImageWithUrl:url success:^(UIImage * _Nonnull result) {
        [indicator stopAnimating];
        [_img setImage:result];
    } failure:^(NSError * _Nullable e) {
        [indicator stopAnimating];
    }];
}

-(void)fillLabels{
    
    
    NSArray *description = [[LocationHelper sharedInstance].current safeValueForKeyPath:@"weather"];
    NSString *des = [[description firstObject]valueForKey:@"description"];
    
    NSString *temp = [[[LocationHelper sharedInstance].current safeValueForKeyPath:@"main.temp"]stringValue];
    NSString *celsius = [NSString stringWithFormat:@"%@ \u00B0C", temp];
    
    _locationNameLabel.text = [LocationHelper sharedInstance].current[@"name"];
    _temp.text =  celsius;
    _des.text = des;
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
