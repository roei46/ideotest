//
//  ThirdViewController.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 12/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "ThirdViewController.h"
#import "LocationHelper.h"
#import "OpenWeatherMapAPI.h"
#import "Constants.h"
#import "NSDictionary+Ideo.h"
#import "ModalViewController.h"
#import "AppUtils.h"
#import "AlertHelper.h"
#import "ViewCity.h"

@interface ThirdViewController ()<loadModalDelegate>

@end

@implementation ThirdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationNameHasChanged:) name:NOTIFICATION_THIRD_CITY object:nil];
    ViewCity *customNib = [[ViewCity alloc] initWithFrame:[UIScreen mainScreen].bounds];
    customNib.delegate = self;
    [self.view addSubview:customNib];
    
    [OpenWeatherMapAPI requestThirdCityData:^(NSDictionary * _Nonnull result) {
        [LocationHelper sharedInstance].defualtThirdLocation = result;
        //        [self fillLabels];
        //        [self fillImage];
        [customNib fillLabelsWithData:result];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_FIRST_CITY object:nil userInfo:@{}];
        
    } failure:^(NSError * _Nullable e) {
        [AlertHelper showAlertWithTitle:@"Error" andMessage:e.localizedDescription];
    }];
}

-(void)loadNewScreen{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    ModalViewController *modalViewController = (ModalViewController*)[mainStoryboard
                                                                      instantiateViewControllerWithIdentifier: @"ModalViewController"];
    [AppUtils fillModalVcForThirdLocation:modalViewController];
    
    [self presentViewController:modalViewController animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
