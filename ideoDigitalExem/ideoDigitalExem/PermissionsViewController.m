//
//  PermissionsViewController.m
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#import "PermissionsViewController.h"
#import "LocationHelper.h"
#import "Constants.h"
#import "ViewControllerSwitch.h"

@interface PermissionsViewController ()

@end

@implementation PermissionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateButtons];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationPermissionHasChanged:) name:NOTIFICATION_LOCATION_PERMISSION_STATE_CHANGE object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)requestPermissions:(id)sender {
    [[LocationHelper sharedInstance] requestPermission];
}

- (IBAction)enterApplication:(id)sender {
    UITabBarController *destVc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [ViewControllerSwitch loadController:destVc];
}

- (void)locationPermissionHasChanged:(NSNotification *)notification {
    [self updateButtons];
}

-(void)updateButtons {
    //enable the button that loads the next screen
    BOOL hasPermission = [[LocationHelper sharedInstance] hasPermission];
    [self.requestAccessButton setEnabled:!hasPermission];
    [self.continueButton setEnabled:hasPermission];
    
    if (hasPermission) {
        self.instructionsLabel.text = TEXT_PERMISSIONS_ENABLED;
        self.continueButton.enabled = YES;
        self.requestAccessButton.enabled = NO;
    } else {
        self.instructionsLabel.text = TEXT_PERMISSIONS_NOT_ENABLED;
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
