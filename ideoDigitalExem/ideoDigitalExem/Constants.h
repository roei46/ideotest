//
//  Header.h
//  ideoDigitalExem
//
//  Created by Roei Baruch on 11/11/2017.
//  Copyright © 2017 Roei Baruch. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define WEATHERMAP_API_KEY @"&appid=96f594ec6b4ee44ebc556ef828967acb"

#define WEATHER_TEMPERATURE_UNIT @"&units=metric"
#define WEATHER_FIRST_CITY @"q=Chicago"
#define WEATHER_SECOND_CITY @"q=Berlin"
#define WEATHER_THIRD_CITY @"q=Manchester"

#define NOTIFICATION_FIRST_CITY @"firtCity"
#define NOTIFICATION_SECONT_CITY @"secondCity"
#define NOTIFICATION_THIRD_CITY @"thirdCity"

#define OPENWEATHERMAP_API_KEY @"http://api.openweathermap.org/data/2.5/weather?"


#define DEFAULTS_KEY_HAS_VISITED_APP @"hasVisitedApp"
#define CONFIG_KEY_MONITOR_REACHABILITY @"monitorReachability"

#define NOTIFICATION_LOCATION_PERMISSION_STATE_CHANGE @"locationPermissionStateChangedNotification"
#define NOTIFICATION_LOCATION_CHANGED @"locationNameChangedNotification"

#define TEXT_PERMISSIONS_ENABLED @"Location permissions are enabled. Have fun! :)"
#define TEXT_PERMISSIONS_NOT_ENABLED @"Please allow access to your location for proper functionality of the app."
#define TEXT_PERMISSION_RESTRICTED_TITLE @"Uh Oh!"
#define TEXT_PERMISSION_RESTRICTED_MESSAGE @"It looks like we aren't able to enable location permissions from here. Please enable them manually from the settings app."
#define TEXT_OPEN_SETTINGS @"Open Settings"
#define TEXT_MAYBE_LATER @"Maybe Later"
#define TEXT_ALERT_OK @"OK"
#define TEXT_ALERT_CANCEL @"Cancel"
#define TEXT_ALERT_ERROR_TITLE @"Error!"
#define TEXT_ALERT_ERROR_MESSAGE @"Oops! Something went wrong. Please try again."

#endif /* Header_h */
